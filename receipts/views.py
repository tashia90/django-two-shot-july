from django.views.generic import ListView, CreateView
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy


class ReceiptListView(ListView, LoginRequiredMixin):
    model = Receipt
    template_name = "receipts.list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    fields = [
        "vendor",
        "total",
        "tax",
        "date",
        "category",
        "account",
    ]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        return super().form_valid(form)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    context_object_name = "category_list"
    template_name = "receipts/create_category.html"
    fields = ["name"]
    success_url = reverse_lazy("category_list")

    def get_queryset(self):
        return super().get_queryset().filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/account_list.html"
    context_object_name = "account_list"

    def get_queryset(self):
        return super().get_queryset().filter(owner=self.request.user)
